mtr utility tool
================


# What is mtr?

mtr combines the functionality of the 'traceroute' and 'ping' programs in a single network diagnostic tool
License: GPL-2.0 License
Languages: C/C++, Python, Shell,...
Source: https://github.com/traviscross/mtr
Web site: https://www.bitwizard.nl/mtr/


# Example of use in report mode

```Shell
~# mtr --report --show-ips --report-wide --tcp -P 443 google.com
Start: 2021-07-23T12:06:39+0200
HOST: myhost.example.com                         Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- ***.***.***.***                             0.0%    10    0.6   0.5   0.4   0.7   0.1
  2.|-- ***.***.***.***                             0.0%    10    2.9   2.6   2.5   2.9   0.2
  3.|-- ???                                        100.0    10    0.0   0.0   0.0   0.0   0.0
  4.|-- 62.34.2.156                                30.0%    10  7140. 2636.  13.5 7140. 3262.0
  5.|-- 62.34.2.155                                 0.0%    10   13.7  13.7  13.0  14.6   0.4
  6.|-- be11.cbr01-cro.net.bbox.fr (212.194.171.2)  0.0%    10   15.0  15.0  14.0  17.0   0.8
  7.|-- 62.34.2.58                                 80.0%    10  3098. 5109. 3098. 7119. 2843.3
  8.|-- 72.14.204.68                                0.0%    10   14.8  14.7  14.0  16.7   0.8
  9.|-- 108.170.244.240                             0.0%    10   15.0  15.2  14.0  18.6   1.3
        108.170.245.6
        108.170.245.5
        108.170.244.177
        108.170.244.198
 10.|-- 209.85.251.179                              0.0%    10   15.4  17.2  15.0  29.5   4.4
        108.170.230.205
        108.170.233.114
        209.85.253.217
        108.170.238.162
 11.|-- 209.85.252.148                              0.0%    10  3137. 750.8  23.8 3137. 1293.8
        209.85.251.176
        209.85.253.184
        216.239.50.186
 12.|-- 108.170.236.246                             0.0%    10   37.1  37.2  36.3  38.1   0.6
        216.239.41.56
        108.170.236.250
        216.239.56.68
 13.|-- 74.125.242.225                              0.0%    10   38.3  38.0  36.8  38.8   0.6
        74.125.242.241
 14.|-- 216.239.35.183                              0.0%    10   61.9  40.1  37.4  61.9   7.7
        209.85.255.243
 15.|-- bud02s27-in-f14.1e100.net (172.217.19.110)  0.0%    10   48.1  38.3  36.5  48.1   3.5
~#
```


# Internet resources

- [How to: MTR – Understanding and Troubleshooting Network Connectivity](https://www.atlantic.net/vps-hosting/how-to-mtr-understanding-troubleshooting-network-connectivity/):  December 24, 2015 by Atlantic.Net NOC
- [What is MTR & How to Use to Troubleshoot & Test your Connections](What is MTR & How to Use to Troubleshoot & Test your Connections):  Marc Wilson Last Updated : 2021-07-22
